#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <string>
#include "ITModule.h"
#include "parser.h"

#pragma mark Main Program

#define FACTORY_PACK "factory.it"

int main(int argc, char** argv) {
  bool compile = true;
  newstr(inpfn);
  newstr(oupfn);
  newstr(packfn);
  
  char biglong[65536];
  char* bl = biglong;

  strcpy(packfn,FACTORY_PACK);

  strcpy(inpfn,argv[1]);
  char* xpo = oupfn;
  xpo += sprintf(xpo,"%s.it",argv[1]);

  FILE *inf = fopen(inpfn,"r");
  it::Module *mod = new it::Module();
  char str[4096];
  
  int mode = READ_MODE_CMD; //command reading
  int bmode = BLOCK_MODE_NONE; //null mode
  int amode = ADDRESS_MODE_NONE; //addressing mode
  
  std::vector<int> chtargets;
  std::map<int,mcmdcontainer> gates;
  std::map<int,mcmdcontainer> subs;
  std::vector<mcmdpattern> patterns;
  
  int patindex = -1;
  
  mglobalinfo glob;
  mchinfo chan[128];
  
  int gatetarget;
  int subtarget;
  int l = 0;
  
  bool single_chan = false;
  bool alt_chan = false;
  int ch_filter = -1;
  int single_chan_index = 0;
  
#define get_target_chinfos(v) \
  std::vector<mchinfo*> v; \
  if (single_chan) { \
    if (single_chan_index >= chtargets.size()) { \
      printf("YOU ARE ADDRESSING TOO MANY CHANNELS.\n"); \
      exit(2); \
    } \
    int theind = chtargets[single_chan_index++]; \
    printf("(Ch%d) ",theind); \
    v.push_back(&chan[theind]); \
  } \
  else { \
    foreach(it, chtargets) v.push_back(&chan[*it]); \
  }
 

  
#define add_target_chcontainers(v,adv) \
  if (single_chan) { \
    if (single_chan_index >= chtargets.size()) { \
    printf("YOU ARE ADDRESSING TOO MANY CHANNELS.\n"); \
    exit(2); \
    } \
    int theind = chtargets[single_chan_index]; \
    if (adv) single_chan_index++; \
    printf("(Ch%d) ",theind); \
    v.push_back(&patterns[patindex].c[theind]); \
  } \
  else { \
    foreach(it, chtargets) v.push_back(&patterns[patindex].c[*it]); \
  }
    
  newstr(addrstr);

  //new pattern
  patindex++;
  mcmdpattern p;
  patterns.push_back(p);
  bmode = BLOCK_MODE_DATA; //init block
  printf("New Pattern %d\n",patindex);

  amode = ADDRESS_MODE_CHANNEL; //channel

  chtargets.clear();
  chtargets.push_back(1); //select channel 1
  strcpy(addrstr,"Ch1");
  chan[1].active = true;
  while(fgets(str,sizeof(str),inf) != NULL)
  {
    bl += sprintf(bl,"%s",str);
    // strip trailing '\n' if it exists
    
    
    int len = strlen(str);
    for (int i = 0; i < strlen(str); i++) {
      if(str[i] == '\n' || str[i] == ';') {
        str[i] = 0;
        len = i;
        break;
      }
    }
    
    newstr(cmd);
    int c = 0;
    int p = 0;
    mcmd cm;
    while (p < len) {
      memset(cmd,0,256);
      for (int i = p; i < len; i++) {
        char t = str[i];
        bool ws = is_whitespace(t);
        if (mode == READ_MODE_CMD) { //command reading
          if (ws) break;
          cmd[c++] = t;
          cmd[c] = 0;
          cm.parse(cmd);
          if (cm.cid >= 0) {
            p = i+1;
            c = 0;
            printf("Ln%d Co%d: ",l,p-strlen(cmd)+1);
            printf ("CMD %d-%d: ",cm.ctype,cm.cid);
            if (cm.ctype == CT_BLOCK) {
              printf("BLOCK ");
              if (cm.cid == CMD_BLOCK_PACK) { //rest of the line is filename
                printf("PACK\n");
                char fn[256];
                memset(fn,0,256);
                //eat whitespace
                while (p < len) {
                  char t = str[p];
                  if (!is_whitespace(t)) break;
                  p++;
                }
                //read name
                i = 0;
                while (p < len) {
                  char t = str[p];
                  fn[i] = t;
                  fn[i+1] = 0;
                  i++;
                  p++;
                }
                strcpy(packfn,fn);
              }
              else if (cm.cid == CMD_BLOCK_NAME) { //rest of the line is filename
                printf("NAME\n");
                char fn[256];
                memset(fn,0,256);
                //eat whitespace
                while (p < len) {
                  char t = str[p];
                  if (!is_whitespace(t)) break;
                  p++;
                }
                //read name
                i = 0;
                while (p < len) {
                  char t = str[p];
                  fn[i] = t;
                  fn[i+1] = 0;
                  i++;
                  p++;
                }
                printf("Name: %s...\n",fn);
                glob.name = fn;
              }
              else if (cm.cid == CMD_BLOCK_OUTF) { //rest of the line is filename
                printf("OUTF\n");
                char fn[256];
                memset(fn,0,256);
                //eat whitespace
                while (p < len) {
                  char t = str[p];
                  if (!is_whitespace(t)) break;
                  p++;
                }
                //read name
                i = 0;
                while (p < len) {
                  char t = str[p];
                  fn[i] = t;
                  fn[i+1] = 0;
                  i++;
                  p++;
                }
                printf("Output File: %s...\n",fn);
                strcpy(oupfn,fn);
              }
              else if (cm.cid == CMD_BLOCK_INIT) {
                amode = ADDRESS_MODE_GLOBAL; //global addressing mode
                bmode = BLOCK_MODE_INIT; //init block
                printf("INIT\n");
              }
              else if (cm.cid == CMD_BLOCK_ENDM) {
                printf("ENDM\n");
                compile = true;
              }
              else if (cm.cid == CMD_BLOCK_DATA) {
                patindex++;
                mcmdpattern p;
                patterns.push_back(p);
                amode = ADDRESS_MODE_PATTERN; //global addressing mode
                bmode = BLOCK_MODE_DATA; //init block
                printf("New Pattern %d\n",patindex);
              }
              
            }
            else if (cm.ctype == CT_GLOBAL) {
              printf("GLOBAL ");
              if (cm.cid == CMD_GLOBAL_RAND) {
                glob.randomize = true;
              }
              else {
                cm.scanval(str,&p);
                
                if (cm.cid == CMD_GLOBAL_PR) {
                  printf("Set pattern size to: %d rows\n",cm.cval);
                  glob.pattern_rows = cm.cval;
                }
                else if (cm.cid == CMD_GLOBAL_PPQN) {
                  printf("Set PPQN to: %d ticks\n",cm.cval);
                  glob.ppqn = cm.cval;
                }
                else if (cm.cid == CMD_GLOBAL_IT) {
                  printf("Set initial tempo to: %d beats per minute\n",cm.cval);
                  glob.init_tempo = cm.cval;
                }
                else if (cm.cid == CMD_GLOBAL_IS) {
                  printf("Set initial row speed to: %d rows\n",cm.cval);
                  glob.init_row_speed = cm.cval;
                  glob.row_speed = cm.cval;
                }
                else if (cm.cid == CMD_GLOBAL_IG) {
                  printf("Set initial global volume to: %d\n",cm.cval);
                  glob.global_vol = cm.cval;
                }
              }
            }
            else if (cm.ctype == CT_ADDRESSING) {
              printf("ADDRESS ");
              if (cm.cid == CMD_ADDRESSING_CH) {
                cm.scanval(str,&p);
                chtargets.clear();
                chtargets.push_back(cm.cval);
                while (str[p] == ',' || str[p] == '-') {
                  bool rng = (str[p] == '-');
                  p++;
                  int oldval = cm.cval;
                  cm.scanval(str,&p);
                  if (rng) {
                    for (int rngi = oldval + 1; rngi < cm.cval; rngi++) {
                      chtargets.push_back(rngi);
                    }
                  }
                  chtargets.push_back(cm.cval);
                }
                char *po = addrstr;
                po+=sprintf(po,"Ch");
                for (int ch = 0; ch < chtargets.size(); ch++) {
                  chan[chtargets[ch]].active = true;
                  po+=sprintf(po,"%d",chtargets[ch]);
                  if (ch+1 < chtargets.size()) {
                    po+=sprintf(po,",");
                  }
                }
                printf("Set target to channel: %s\n",addrstr);
                amode = ADDRESS_MODE_CHANNEL; //channel
              }
              else if (cm.cid == CMD_ADDRESSING_GA) {
                cm.scanval(str,&p);
                chtargets.clear();
                chtargets.push_back(cm.cval+64);
                while (str[p] == ',' || str[p] == '-') {
                  bool rng = (str[p] == '-');
                  p++;
                  int oldval = cm.cval;
                  cm.scanval(str,&p);
                  if (rng) {
                    for (int rngi = oldval + 1; rngi < cm.cval; rngi++) {
                      chtargets.push_back(rngi+64);
                    }
                  }
                  chtargets.push_back(cm.cval+64);
                }
                char *po = addrstr;
                po+=sprintf(po,"Ch");
                for (int ch = 0; ch < chtargets.size(); ch++) {
                  chan[chtargets[ch]].active = true;
                  po+=sprintf(po,"%d",chtargets[ch]);
                  if (ch+1 < chtargets.size()) {
                    po+=sprintf(po,",");
                  }
                }
                printf("Set target to channel: %s\n",addrstr);
                amode = ADDRESS_MODE_CHANNEL; //channel
              }

              else if (cm.cid == CMD_ADDRESSING_SB) {
                cm.scanval(str,&p);
                subtarget = cm.cval;
                mcmdcontainer s;
                subs[subtarget] = s;
                char *po = addrstr;
                po+=sprintf(po,"Sb%d",subtarget);
                printf("Set target to sub: %s\n",addrstr);
                amode = ADDRESS_MODE_SUB;
              }
              else if (cm.cid == CMD_ADDRESSING_OPEN_PAREN) {
                single_chan = true;
                single_chan_index = 0;
                printf("Single Channel Iterate.\n");
              }
              else if (cm.cid == CMD_ADDRESSING_CLOSE_PAREN) {
                printf("End Single Channel Iterate.\n");
                if (single_chan_index < chtargets.size()) {
                  printf("That is wrong. You did not address all channels.\n");
                  exit(2);
                }
                single_chan = false;
              }
              else if (cm.cid == CMD_ADDRESSING_COLON) {
                printf("Iterate Skip\n");
                single_chan_index++;
              }
            }
            else if (cm.ctype == CT_PATTERN) {
              printf("PATTERN %d: ",patindex);
              if (cm.cid == CMD_PATTERN_LOOP) {
                printf("Set this pattern as the song loop point.\n");
                glob.loop_index = patindex;
              }
              else if (cm.cid == CMD_PATTERN_RP) {
                cm.scanval(str, &p);
                printf("Repeat Pattern %d.\n",cm.cval);
                patterns.push_back(patterns[cm.cval]);
                patindex++;
              }
            }
            else if (cm.ctype == CT_CHANNELINIT) {
              printf("CHANNELINIT %s: ",addrstr);
              get_target_chinfos(cv)
              
              cm.scanval(str,&p);
              if (cm.cid == CMD_CHANNELINIT_IP) {
                printf("Set initial pan to: %d\n",cm.cval);
                foreach(it,cv) (*it)->init_pan = cm.cval;
              }
              else if (cm.cid == CMD_CHANNELINIT_IV) {
                printf("Set initial vol to: %d\n",cm.cval);
                foreach(it,cv) (*it)->init_vol = cm.cval;
              }
              
            }
            else { //all other commands
              //  CT_DATA,CT_SHORT,CT_DATATRIGGER,CT_NOTES,CT_SPEC,CT_TRIGGERS
              std::vector<mcmdcontainer*> dests;
              printf("%s: ",addrstr);
              cm.rowend = (cm.ctype == CT_NOTES);
              bool adv = cm.rowend || (cm.ctype == CT_DATA && cm.cid == CMD_DATA_SB);

              if (amode == ADDRESS_MODE_CHANNEL) {
                add_target_chcontainers(dests, adv);
              }
              else if (amode == ADDRESS_MODE_GATE) {
                dests.push_back(&gates[gatetarget]);
              }
              else if (amode == ADDRESS_MODE_SUB) {
                dests.push_back(&subs[subtarget]);
              }
              
              //these commands have values
              if (cm.ctype == CT_DATA && cm.cid == CMD_DATA_EXC) {
                cm.scanletter(str,&p);
                cm.scanval(str,&p);
              }
              else if (cm.ctype == CT_DATA || cm.ctype == CT_SHORT ||
                  (cm.ctype == CT_NOTES && cm.cid == CMD_NOTE_N) ||
                  (cm.ctype == CT_DATATRIGGER && cm.cid == CMD_DATATRIGGER_CLOSE_SQBR) ||
                  (cm.ctype == CT_SPEC && cm.cid == CMD_SPEC_AMPERSAND) ||
                  (cm.ctype == CT_SPEC && cm.cid == CMD_SPEC_PD) ||
                  (cm.ctype == CT_SPEC && cm.cid == CMD_SPEC_PU)
                  ) {
                cm.scanval(str,&p);
              }
              else if (cm.ctype == CT_NOTES && (cm.cid >= CMD_NOTE_C) && (cm.cid <= CMD_NOTE_B)) {
                //scan for sharp
                cm.scansharp(str,&p);
                cm.scanval(str,&p);
              }
              if (cm.ctype == CT_TRIGGERS && cm.cid == CMD_TRIGGER_CLEAR) {
                printf("CLEAR TARGET. ");
                foreach(it,dests) (*it)->d.clear();
              }
              else {
                foreach(it,dests) (*it)->d.push_back(cm);
              }
              printf("\n");
            }
            p--;
            break;
          }
        }
      }
      p++;
    }
    l++;
    if (c > 0) {
      printf("Unknown Command %s.\n",cmd);
      break;
    }
    
  }
  fclose(inf);
  printf("%d lines parsed.\n",l);


  if (strlen(packfn) > 0) { //load pack
      printf("Loading pack %s...\n",packfn);
      FILE *mf = fopen(packfn,"rb");
      mod->Read(mf);
      fclose(mf);
      printf("Pack %s loaded.\n",packfn);
  }
  else {
    printf("No pack specified.\n");
    exit(2);
  }

#pragma mark IT Writer
  if (compile) {



    printf("Creating Module\n");
    mod->clearPatterns();
    mod->sequence.clear();
    //initial settings
    mod->name = glob.name;
    mod->initSpeed = glob.init_row_speed;
    mod->initTempo = glob.init_tempo;
    mod->globalVolume = glob.init_global_vol;
    for (int i = 0; i < 64; i++) {
      mod->initChannelVolume[i] = chan[i].init_vol;
      mod->initChannelPan[i] = chan[i].init_pan + 0x20;
    }
    
    it::Pattern* itpat;
    it::Pattern::Row* itrow;
    int itpatindex = -1;
    int itrowindex = 0;
    bool advance = false;
    for (int pi = 0; pi < patterns.size(); pi++) {
      printf( "Render Pattern %d\n",pi);

      
      if (itpatindex >= 0 && itrowindex < glob.pattern_rows) {
        itpat->row(0, itrowindex-1)->setCommand('C', 0);
      }
      itpatindex++;
      itrowindex = 0;
      mod->patterns[itpatindex] = new it::Pattern();
      mod->patterns[itpatindex]->setSize(glob.pattern_rows);
      mod->sequence.push_back(itpatindex);
      mod->message = biglong;
      mcmdpattern* pat = &patterns[pi];
      pat->itpatindex = itpatindex;
      pat->itrowindex = itrowindex;
      bool patternbreak = false;
      while (true) {
  //      printf( "Row(%d) %d:%d\n",pi,itpatindex,itrowindex);
    
        bool allFinished = true;
        for (int i = 0; i < 128; i++) {
          
          if (chan[i].active && (!chan[i].notesettings.finished || chan[i].notesettings.skip_index > 0) &&
              pat->c[i].d.size() > 0
              ) {
            allFinished = false;
            break;
          }
          
        }
        for (int i = 0; i < 128; i++) {
          if (chan[i].notesettings.breaking && chan[i].notesettings.skip_index == 0) { //chan[i].notesettings.row_skip -1) {
            patternbreak = true;
            break;
          }
        }
        if (allFinished || patternbreak) break; //end of pattern
        
        if (itrowindex >= glob.pattern_rows) {
          itpatindex++;
          itrowindex = 0;
          mod->patterns[itpatindex] = new it::Pattern();
          mod->patterns[itpatindex]->setSize(glob.pattern_rows);
          mod->sequence.push_back(itpatindex);
        }

        
        itpat = mod->patterns[itpatindex];
        
        //parse one row
        bool qpbreak = false;
        for (int i = 0; i < 128; i++) {
          if (chan[i].active && pat->c[i].d.size() > 0) {
  //          printf( "Ch%d Row(%d) %d:%d\n",i,pi,itpatindex,itrowindex);
  /*          if (chan[i].notesettings.cmd_index < pat->c[i].d.size()) {
              chan[i].notesettings.finished = true;
            }
            else {*/
            if (true) {

              itrow = itpat->row(i % 64, itrowindex);
              mchinfo* ch = &chan[i];
              mcmdcontainer* ctr = &pat->c[i];
              mchinfo::mnotesettings* chnt = &ch->notesettings;
              bool sublev = true;
              bool newvol = false;
              bool newvib = false;
              bool newport = false;
              bool newpitch = false;
              bool newcut = false;
              if (chnt->skip_index == 0) {
                bool rowDone = false;
                while (chan[i].notesettings.cmd_index < pat->c[i].d.size()) { //command reading

                  //fetch
                  mcmd* cmd;
                  int sublevel = -1;
                  mcmdcontainer* actr;
                  int* acmd_index;
                  msubinfo* csub = NULL;
                  if (chnt->subs.size() > 0) {
                    sublevel = chnt->subs.size()-1;
                    csub = &chnt->subs[sublevel];
                    actr = csub->ctr;
                    acmd_index = &csub->cmd_index;
                  }
                  else {
                    actr = ctr;
                    acmd_index = &chnt->cmd_index;
                  }
                  cmd = &actr->d[*acmd_index];
                  
                  printf ("Ch%d :",i);
                  
                  //parse
                  if (rowDone) sublev = false;
                  if (cmd->ctype == CT_DATATRIGGER) {
                    if (cmd->cid == CMD_DATATRIGGER_OPEN_SQBR) { //loop start
                      if (rowDone) break;
                      mloopinfo ll;
                      chnt->loops.push_back(ll);
                      int looplevel = chnt->loops.size() - 1;
                      mloopinfo* li = &chnt->loops[looplevel];
                      li->start_index = *acmd_index;
                      printf("Loop Start [%d]\n",looplevel);
                    }
                    else if (cmd->cid == CMD_DATATRIGGER_CLOSE_SQBR) { //loop end
                      sublev = true;
                      int looplevel = chnt->loops.size() - 1;
                      mloopinfo* li = &chnt->loops[looplevel];
                      li->end_index = *acmd_index;
                      if (li->loop_count < 0) { //unset or infinite
                        li->loop_count = cmd->cval - 2;
                        *acmd_index = li->start_index;
                        if (li->loop_count < 0) { //inifinite loop
                          chnt->finished = true;
                          printf("Infinite Loop - ");
                        }
                      }
                      else if (li->loop_count > 0) { //more loops to go
                        *acmd_index = li->start_index;
                        li->loop_count--;
                      }
                      else { //endof loop
                        chnt->loops.erase(chnt->loops.begin() + looplevel);
                      }                  
                      printf("Loop End [%d] (%d remaining)\n",looplevel,li->loop_count+1);
                    }
                    else if (cmd->cid == CMD_DATATRIGGER_PIPE) { //ds to coda
                      int looplevel = chnt->loops.size() - 1;
                      mloopinfo* li = &chnt->loops[looplevel];
                      li->coda_trigger_index = *acmd_index;
                      if (li->loop_count == 0) { //jump to end
                        *acmd_index = li->end_index;
                        chnt->loops.erase(chnt->loops.begin() + looplevel);
                        printf("Loop Coda [%d]\n",looplevel);
                        sublev = true;
                      }
                      printf("Loop Coda Met, Ending [%d]\n",looplevel);
                    }
                    else if (cmd->cid == CMD_DATATRIGGER_LESS_THAN) { //octave down
                      if (rowDone) break;
                      chnt->octave--;
                      if (chnt->octave < 0) chnt->octave = 0;
                      printf("Octave Down\n");
                    }
                    else if (cmd ->cid == CMD_DATATRIGGER_GREATER_THAN) { //octave up
                      if (rowDone) break;
                      chnt->octave++;
                      if (chnt->octave > 15) chnt->octave = 15;
                      printf("Octave Up\n");
                    }
                    else if (cmd ->cid == CMD_DATATRIGGER_CUT) { //pattern cut
                    //  if (rowDone) break;
//                      qpbreak = true;
                      chnt->breaking = true;
//                      break;
                      
                    }
                  }
                  else if (cmd->ctype == CT_DATA) {
                    if (rowDone) break;
                    if (cmd->cid == CMD_DATA_SB) { //call sub
                      printf("Call Sub %d ",cmd->cval);
                      if (subs.count(cmd->cval)>0) { //sub exists
                        msubinfo ss;
                        chnt->subs.push_back(ss);
                        sublevel = chnt->subs.size()-1;
                        msubinfo *si = &chnt->subs[sublevel];
                        csub = si;
                        si->cmd_index = -1; //hax
                        si->sub_index = cmd->cval;
                        si->ctr = &subs[si->sub_index];
                        printf("In Sub [%d]\n",sublevel);
                      }
                    }
                    else if (cmd->cid == CMD_DATA_PERCENT) { //instrument
                      cmd->applyval(&chnt->inst);
                      printf("Set Inst %d\n",chnt->inst);
                    }
                    else if (cmd->cid == CMD_DATA_RS) { //change row skip
                      cmd->applyval(&chnt->row_skip);
                      printf("Set Row Skip %d\n",chnt->row_skip);
                    }
                    else if (cmd->cid == CMD_DATA_TR) { //change row skip
                      cmd->applyval(&chnt->transpose);
                      printf("Set Transpose %d\n",chnt->transpose);
                    }
                    else if (cmd->cid == CMD_DATA_Z) { //change row skip
                      
                      cmd->applyval(&chnt->filter);
                      if (chnt->filter < 0) chnt->filter = 0;
                      if (chnt->filter > 127) chnt->filter = 127;
                      printf("Set Filter %d\n",chnt->filter);
                      itrow->setCommand('Z', chnt->filter);
                    }
                    else if (cmd->cid == CMD_DATA_G) { //change row skip
                      cmd->applyval(&chnt->port_speed);
                      printf("Set Port Speed %d\n",chnt->port_speed);
                    }
                    else if (cmd->cid == CMD_DATA_H) { //change row skip
                      cmd->applyval(&chnt->vib_setting);
                      printf("Set Vibrato %d\n",chnt->vib_setting);
                    }
                    else if (cmd->cid == CMD_DATA_F) { //change row skip
                      cmd->applyval(&chnt->pitch_speed);
                      printf("Set Vibrato %d\n",chnt->pitch_speed);
                    }
                    else if (cmd->cid == CMD_DATA_IV) { //inital vol
                      cmd->applyval(&chnt->init_note_vol);
                      if (chnt->init_note_vol > 0x40) chnt->init_note_vol = 0x40;
                      else if (chnt->init_note_vol < 0) chnt->init_note_vol = 0x0;
                      printf("Set Init Note Vol %d\n",chnt->init_note_vol);
                    }
                    else if (cmd->cid == CMD_DATA_V) { //curr vol
                      cmd->applyval(&chnt->note_vol);
                      if (chnt->note_vol > 0x40) chnt->note_vol = 0x40;
                      else if (chnt->note_vol < 0) chnt->note_vol = 0x0;
                      printf("Set Cur Note Vol %d\n",chnt->note_vol);
                      newvol = true;
                      itrow->volume = chnt->note_vol;
                    }
                    else if (cmd->cid == CMD_DATA_SS) { //curr vol
//                      printf("Set Cur Note Vol S %d\n",chnt->note_vol);
                      newvol = true;
                      itrow->volume = it::VPVolumeSlideDownStart + cmd->cval;
                    }
                    else if (cmd->cid == CMD_DATA_L) { //curr vol
                      int l = cmd->cval;
                      chnt->row_skip = ((glob.ppqn / glob.row_speed) * 4) / l;
                      printf("Set Row Skip %d\n",chnt->row_skip);
                    } 
                    else if (cmd->cid == CMD_DATA_UNDERSCORE) { //cut after
                      int l = cmd->cval;
                      if (l == 0 || !cmd->hasval) l = 1;
                      if (l < glob.row_speed) {
                        printf("Note cut after %d ticks\n",l);
                        itrow->setCommand('S',0xC0 + l);
                      }
                      else {
                        printf("Note cut (flow on) after %d ticks\n",l);
                        chnt->note_off_counter = l / glob.row_speed;
                        chnt->note_off_value = l % glob.row_speed;
                        newcut = true;
                      }
                    }
                    else if (cmd->cid == CMD_DATA_QUO) { //cut after
                      int l = cmd->cval;
                      if (l == 0 || !cmd->hasval) l = 1;
                      if (l < glob.row_speed) {
                        printf("Note delay after %d ticks\n",l);
                        itrow->setCommand('S',0xD0 + l);
                      }
                    }
                    else if (cmd->cid == CMD_DATA_EXC) { //cut after
                      itrow->setCommand(cmd->cchar,cmd->cval);
                      printf("IT Effect %c%02X\n",cmd->cchar,cmd->cval);
                    }                  }
                  else if (cmd->ctype == CT_SPEC) {
                    if (rowDone) break;
                    if (cmd->cid == CMD_SPEC_TILDE) { //vibrator
                      chnt->vibrato_enabled = true;
                      newvib = true;
                    }
                    else if (cmd->cid == CMD_SPEC_AMPERSAND) { ////port
                      chnt->port_enabled = true;
                      newport = true;
//                      chnt->autoport = cmd->cval;
                    }
                    else if (cmd->cid == CMD_SPEC_PU) { ////port
                      chnt->pitch_direction = 1;
                      newpitch = true;
                      chnt->autoport = cmd->cval;
                    }
                    else if (cmd->cid == CMD_SPEC_PD) { ////port
                      chnt->pitch_direction = -1;
                      newpitch = true;
                      chnt->autoport = cmd->cval * -1;
                    }
                  }
                  else if (cmd->ctype == CT_SHORT) { //short
                    if (cmd->cid == CMD_SHORT_O) {
                      cmd->applyval(&chnt->octave);
                      printf("Set Octave %d\n",chnt->octave);
                    }
                  }
                  else if (cmd->ctype == CT_NOTES) { //notes
                    if (rowDone) break;
                    rowDone = true;
                    if (cmd->cid == CMD_NOTE_SLASH) { //note off
                      itrow->note = it::NoteNumberOff;
                      itrow->instrument = chnt->inst;
                      printf("Note Off\n");
                    }
                    else if (cmd->cid == CMD_NOTE_CARET) { //note cut
                      itrow->note = it::NoteNumberCut;
                      itrow->instrument = chnt->inst;
                      printf("Note Cut\n");
                    }
                    else if (cmd->cid == CMD_NOTE_BACKSLASH) { //repeat note
                      itrow->note = chnt->current_note + chnt->transpose;
                      itrow->instrument = chnt->inst;
                      if (!newvol) {
                        itrow->volume = chnt->init_note_vol;
                        chnt->note_vol = chnt->init_note_vol;
                      }
                      if (!newport) chnt->port_enabled = false;
                      if (!newvib) chnt->vibrato_enabled = false;
                      if (!newpitch) chnt->pitch_direction = 0;
                      if (!newcut) chnt->note_off_counter = -1;
                      printf("Note Repeat (%d)\n",chnt->current_note);
                    }
                    else if (cmd->cid >= CMD_NOTE_C && cmd->cid <= CMD_NOTE_B) { //normal note
                      const int notemap[7] = { 0, 2, 4, 5, 7, 9, 11 };
                      int nn = cmd->cid - CMD_NOTE_C;
                      int o;
                      if (cmd->hasval) o = cmd->cval;
                      else o = chnt->octave;
                      int nid = o * 12 + notemap[nn] + ((cmd->csharp)?1:0);

                      if (nid >= 0x9C) { //drums
                        int x = nid - 0x9C + 0xD0;
                        printf("Drum Note %d ",x);
                        if (subs.count(x)>0) { //sub exists
                          rowDone = false;
                          msubinfo ss;
                          chnt->subs.push_back(ss);
                          sublevel = chnt->subs.size()-1;
                          msubinfo *si = &chnt->subs[sublevel];
                          csub = si;
                          si->drum = true;
                          si->restore_oct = chnt->octave;
                          si->cmd_index = -1; //hax
                          si->sub_index = x;
                          si->ctr = &subs[x];
                          printf("In Sub [%d]\n",sublevel);
                        }
                      }
                      else {
                        chnt->current_note = nid;
                        itrow->note = nid + chnt->transpose;
                        itrow->instrument = chnt->inst;
                        printf("Note On (%d)\n",chnt->current_note);
                        if (!newvol) {
                          itrow->volume = chnt->init_note_vol;
                          chnt->note_vol = chnt->init_note_vol;
                        }
                        if (!newport) chnt->port_enabled = false;
                        if (!newvib) chnt->vibrato_enabled = false;
                        if (!newpitch) chnt->pitch_direction = 0;
                        if (!newcut) chnt->note_off_counter = -1;
                      }
                    }
                  }
                  else {
                    printf("Unparsed Command %d-%d\n",cmd->ctype,cmd->cid);
                    
                  }
                  //advance
                  while (sublevel >= 0) {
                    csub->cmd_index++;
                    if (csub->cmd_index >= csub->ctr->d.size()) {
                      //end of sub
                      printf("End Sub [%d] #%d\n",sublevel,csub->sub_index);
                      if (csub->drum) {
                        printf("Reset Drum Octave\n");
                        chnt->octave = csub->restore_oct;
                      }
                      chnt->subs.erase(chnt->subs.begin() + sublevel);
                      sublevel = chnt->subs.size()-1;
                      csub = &chnt->subs[sublevel];
                    }
                    else {
                      break;
                    }
                  }
                  
                  if (sublevel < 0) {
                    chnt->cmd_index++;
                    if (chnt->cmd_index >= ctr->d.size()) {
                      printf("End of channel commands\n");
                      chnt->finished = true;
                      break;
                    }
                  }
                  
                }
                //;
  //              advance = advance || rowDone;
              }
              
              if (chnt->note_off_counter == 0) {
                chnt->note_off_counter = -1;
                if (chnt->note_off_value > 0) {
                  itrow->setCommand('S',0xC0 + chnt->note_off_value);
                }
                else {
                  itrow->note = it::NoteNumberCut;
                }
              }
              else if (chnt->pitch_direction != 0) {
                if (newpitch) {
                  chnt->pitch_step =  ((float)chnt->autoport*16.0f) / (float)chnt->pitch_speed / (float)(glob.row_speed-1);
                  chnt->fine_note = chnt->pitch_speed;
                  
                  
                  
                  
                }
//                chnt->fine_note += (chnt->pitch_direction) * chnt->pitch_step * (glob.row_speed - 1);
                itrow->setCommand((chnt->pitch_direction > 0 ? 'F' : 'E'), abs(chnt->pitch_step));
                chnt->fine_note --;
                if (chnt->fine_note <= 0) {
                  chnt->pitch_direction = 0;
                }
              }
              else if (chnt->vibrato_enabled) {
                itrow->setCommand('H', chnt->vib_setting);
              }
              else if (chnt->port_enabled) {
/*                if (!newport && chnt->autoport != 0) {
                  itrow->note = chnt->current_note + chnt->transpose + chnt->autoport;
                  chnt->autoport = 0;
                }*/
                itrow->setCommand('G', chnt->port_speed);
              }
              
              if (chnt->note_off_counter > 0) {
                chnt->note_off_counter--;
              }
              

              
              ch->notesettings.skip_index = (ch->notesettings.skip_index + 1) % ch->notesettings.row_skip;
              //process per row events
            
            
            
            }
          }
          else {
            chan[i].notesettings.finished = true;
          }
        }
  //      if (advance) {
         itrowindex++;
      //    advance = false;
    //    }
      }
      //cleanup
      for (int i = 0; i < 128; i++) {
        chan[i].notesettings.breaking = false;
        
        chan[i].notesettings.finished = false;
        chan[i].notesettings.cmd_index = 0;
        chan[i].notesettings.skip_index = 0;
        chan[i].notesettings.loops.clear();
        chan[i].notesettings.subs.clear();
      }
    
    }
    if (glob.loop_index >= 0) {
      itpat->row(0,itrowindex-1)->setCommand('B', patterns[glob.loop_index].itpatindex);
      if (patterns[glob.loop_index].itrowindex) itpat->row(1,itrowindex-1)->setCommand('C', patterns[glob.loop_index].itrowindex);
    }
    else {
      if (itrowindex < glob.pattern_rows) itpat->row(0,itrowindex-1)->setCommand('C', 0);
    }
    printf("Saving .it file...\n");
    FILE *ouf = fopen(oupfn,"wb");
    mod->Write(ouf);
    fclose(ouf);
  }
}
//
//  ImpulseTrackerModule.cpp
//  keyboardtest
//
//  Created by Andrew Lim on 12/05/18.
//  Copyright (c) 2012年 No Limit Zone. All rights reserved.
//


#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include "ITModule.h"


namespace it {
	
	const char NoteNames[12][4] = {
		"C ",
		"C#",
		"D ",
		"D#",
		"E ",
		"F ",
		"F#",
		"G ",
		"G#",
		"A ",
		"A#",
		"B "
	};
	
	

  
	bool Sample::Write(FILE* file) {
		const int NameMax = 26;
		const int DosNameMax = 13;
    
		const uint32_t header = 0x53504D49;
    
		fput<uint32_t>(file,header);
    
		char theDosName[DosNameMax];
		memset(theDosName,0,sizeof(theDosName));
		strcpy(theDosName,this->dosName.c_str());
		theDosName[DosNameMax-1] = '\0';
		fwrite(theDosName,sizeof(char),DosNameMax,file);
    
		fputc(this->globalVolume,file);
		fputc(this->flags,file);
		fputc(this->defaultVolume,file);
    
		char theName[NameMax];
		memset(theName,0,sizeof(theName));
		strcpy(theName,this->name.c_str());
		theName[NameMax-1] = '\0';
		fwrite(theName,sizeof(char),NameMax,file);
    
		fputc(this->flagsConvert,file);
		fputc(this->defaultPan,file);
    
		fput<uint32_t>(file,this->sampleCount);
		fput<uint32_t>(file,this->loopStart);
		fput<uint32_t>(file,this->loopEnd);
		fput<uint32_t>(file,this->rate);
		fput<uint32_t>(file,this->sustainLoopStart);
		fput<uint32_t>(file,this->sustainLoopEnd);
    
		size_t sampleDataPointer = ftell(file);
		fput<uint32_t>(file,0); //placeholder
    
		fputc(this->vibratoSpeed,file);
		fputc(this->vibratoDepth,file);
		fputc(this->vibratoWaveForm,file);
		fputc(this->vibratoRate,file);
		
		if (this->dataSize > 0) {
			size_t ptr = ftell(file);
			fseek(file,sampleDataPointer,SEEK_SET);
			fput<uint32_t>(file,ptr);
			fseek(file,ptr,SEEK_SET);
			fwrite(this->sampleData,sizeof(unsigned char),this->dataSize,file);
		}
		
		return true;
	}
	
	bool Sample::Read(FILE* file) {
		const int NameMax = 26;
		const int DosNameMax = 13;
    
		const uint32_t header = 0x53504D49;
		
		size_t start = ftell(file);
		
		if (fget<uint32_t>(file) != header) return false; //header mismatch
		char theDosName[DosNameMax];
		fread(theDosName,sizeof(char),DosNameMax,file);
		this->dosName = theDosName;
		
		this->globalVolume = fgetc(file);
		this->flags = fgetc(file);
		this->defaultVolume = fgetc(file);		
    
		char theName[NameMax];
		fread(theName,sizeof(char),NameMax,file);
		this->name = theName;
		
		this->flagsConvert = fgetc(file);
		this->defaultPan = fgetc(file);
		
		this->sampleCount = fget<uint32_t>(file);
		this->loopStart = fget<uint32_t>(file);
		this->loopEnd = fget<uint32_t>(file);
		this->rate = fget<uint32_t>(file);
		this->sustainLoopStart = fget<uint32_t>(file);
		this->sustainLoopEnd = fget<uint32_t>(file);
		size_t sampleOffset = fget<uint32_t>(file);
		this->vibratoSpeed = fgetc(file);
		this->vibratoDepth = fgetc(file);
		this->vibratoWaveForm = (VibratoWaveForm)fgetc(file);
		this->vibratoRate = fgetc(file);
		
		this->bytesPerSample = (this->flags & Flag16Bit ? 2 : 1) * (this->flags & FlagStereo ? 2 : 1);
		
		if (this->flags & FlagCompressed) {
			printf("Compressed samples not supported\n");
			return false;
		}
		
		this->dataSize = bytesPerSample * this->sampleCount;
		
		if (this->dataSize > 0) {		
			this->sampleData = new unsigned char[this->dataSize];
			fseek(file,sampleOffset,SEEK_SET);
			fread(this->sampleData,sizeof(unsigned char),this->dataSize,file);
			this->sampleLoaded = true;
		}
		
		return true;
	}
  
	bool Instrument::Envelope::Write(FILE* file) {
		fputc(this->flags,file);
		fputc(this->nodeCount,file);
		fputc(this->loopStartIndex,file);
		fputc(this->loopEndIndex,file);
		fputc(this->sustainLoopStartIndex,file);
		fputc(this->sustainLoopEndIndex,file);
		for (int i = 0; i < NodeMax; i++) {
			fputc(this->nodes[i].value,file);
			fput<uint16_t>(file,this->nodes[i].tick);
		}
		fputc(0,file); //reserved
    
		return true;
	}
  
	bool Instrument::Envelope::Read(FILE* file) {
		this->flags = fgetc(file);
		this->nodeCount = fgetc(file);
		this->loopStartIndex = fgetc(file);
		this->loopEndIndex = fgetc(file);
		this->sustainLoopStartIndex = fgetc(file);
		this->sustainLoopEndIndex = fgetc(file);
		for (int i = 0; i < NodeMax; i++) {
			this->nodes[i].value = fgetc(file);
			this->nodes[i].tick = fget<uint16_t>(file);
		}
		fseek(file,1,SEEK_CUR); //reserved;
		return true;
	}
	
	bool Instrument::Write(FILE* file) {
		const int NameMax = 26;
		const int DosNameMax = 13;
    
		const uint32_t header = 0x49504D49;
    
		fput<uint32_t>(file,header);
    
		char theDosName[DosNameMax];
		memset(theDosName,0,sizeof(theDosName));
		strcpy(theDosName,this->dosName.c_str());
		theDosName[DosNameMax-1] = '\0';
		fwrite(theDosName,sizeof(char),DosNameMax,file);
    
		fputc(this->newNoteAction,file);
		fputc(this->duplicateCheckType,file);
		fputc(this->duplicateCheckAction,file);
		fput<uint16_t>(file,this->fadeOut);
		fputc(this->pitchPanSeparation,file);
		fputc(this->pitchPanCenter,file);
		fputc(this->globalVolume,file);
		fputc(this->defaultPan,file);
		fputc(this->randomVolumeVariation,file);
		fputc(this->randomPanVariation,file);
		fput<uint16_t>(file,this->createVersion);
		fputc(this->sampleCount,file);
		fputc(0,file); //reserved
    
		char theName[NameMax];
		memset(theName,0,sizeof(theName));
		strcpy(theName,this->name.c_str());
		theName[NameMax-1] = '\0';
		fwrite(theName,sizeof(char),NameMax,file);
    
		fputc(this->initFilterCutoff,file);
		fputc(this->initFilterResonance,file);
		fputc(this->midiChannel,file);
		fputc(this->midiProgram,file);
		fput<uint16_t>(file,this->midiBank);
    
		for (int i = 0; i < NoteCount; i++) {
			fputc(this->noteSampleMap[i].noteNumber,file);
			fputc(this->noteSampleMap[i].sampleNumber,file);
		}
    
		for (int i = 0; i < EnvelopeCount; i++) {
			this->envelopes[i].Write(file);
		}
    
		fput<uint32_t>(file,0); //reserved
    
		return true;
	}
  
	bool Instrument::Read(FILE* file) {
		const int NameMax = 26;
		const int DosNameMax = 13;
    
		const uint32_t header = 0x49504D49;
    
		if (fget<uint32_t>(file) != header) return false; //header mismatch
		char theDosName[DosNameMax];
		fread(theDosName,sizeof(char),DosNameMax,file);
		this->dosName = theDosName;
		
		this->newNoteAction = (NewNoteAction)fgetc(file);
		this->duplicateCheckType = (DuplicateCheckType)fgetc(file);
		this->duplicateCheckAction = (DuplicateCheckAction)fgetc(file);
		this->fadeOut = fget<uint16_t>(file);
		this->pitchPanSeparation = fgetc(file);
		this->pitchPanCenter = fgetc(file);
		this->globalVolume = fgetc(file);
		this->defaultPan = fgetc(file);
		this->randomVolumeVariation = fgetc(file);
		this->randomPanVariation = fgetc(file);
		this->createVersion = fget<uint16_t>(file);
		this->sampleCount = fgetc(file);
		fseek(file,1,SEEK_CUR); //reserved
    
		char theName[NameMax];
		fread(theName,sizeof(char),NameMax,file);
		this->name = theName;
    
		this->initFilterCutoff = fgetc(file);
		this->initFilterResonance = fgetc(file);
		this->midiChannel = fgetc(file);
		this->midiProgram = fgetc(file);
		this->midiBank = fget<uint16_t>(file);
		
		for (int i = 0; i < NoteCount; i++) {
			this->noteSampleMap[i].noteNumber = fgetc(file);
			this->noteSampleMap[i].sampleNumber = fgetc(file);
		}
    
		
		for (int i = 0; i < EnvelopeCount; i++) {
			this->envelopes[i].Read(file);
		}
		
		fseek(file,4,SEEK_CUR); //reserved
		
		return true;
	}
	
	bool Pattern::Write(FILE* file) {
		const int ChannelMax = 64;
		uint16_t dataSize = 0;
		size_t dataSizePointer = ftell(file);
		fput<uint16_t>(file,dataSize); //placeholder
		fput<uint16_t>(file,this->rowCount);
		fput<uint32_t>(file,0); //reserved
    
    
		uint8_t cv = 0;
		uint8_t mv = 0;
		
		uint8_t lastMv[ChannelMax];
		uint16_t lastNote[ChannelMax];
		uint8_t lastInstrument[ChannelMax];
		uint8_t lastVolume[ChannelMax];
		uint8_t lastCommand[ChannelMax];
		uint8_t lastCommandValue[ChannelMax];
		
		memset(lastMv,0,sizeof(lastMv));
		memset(lastCommandValue,0,sizeof(lastCommandValue));
		memset(lastNote,0xFF,sizeof(lastNote));
		memset(lastInstrument,0xFF,sizeof(lastInstrument));
		memset(lastVolume,0xFF,sizeof(lastVolume));
		memset(lastCommand,0xFF,sizeof(lastCommand));
    
    
		size_t start = ftell(file);
    
		Row *row;
    
		for (int i = 0; i < rowCount; i++) {
			for (int t = 0; t < ChannelMax; t++) {
				row = &(this->rows[i*ChannelMax+t]);
				if (!row->isEmpty()) {
					cv = t + 1;
					mv = 0;
					if (row->hasNote()) {
						if (row->note == lastNote[t]) mv |= RowMaskLastNote;
						else mv |= RowMaskNote;
					}
					if (row->hasInstrument()) {
						if (row->instrument == lastInstrument[t]) mv |= RowMaskLastInstrument;
						else mv |= RowMaskInstrument;
					}
					if (row->hasVolume()) {
						if (row->volume == lastVolume[t]) mv |= RowMaskLastVolume;
						else mv |= RowMaskVolume;
					}
					if (row->hasCommand()) {
						if (row->command == lastCommand[t] &&
                row->commandValue == lastCommandValue[t]) mv |= RowMaskLastCommand;
						else mv |= RowMaskCommand;
					}
					if (mv == lastMv[t]) {
						fputc(cv,file);
					}
					else {
						fputc(cv | 0x80,file);
						fputc(mv,file);
						lastMv[t] = mv;
					}
					if (mv & RowMaskNote) {
						fputc(row->note,file);
						lastNote[t] = row->note;
					}
					if (mv & RowMaskInstrument) {
						fputc(row->instrument,file);
						lastInstrument[t] = row->instrument;
					}
					if (mv & RowMaskVolume) {
						fputc(row->volume,file);
						lastVolume[t] = row->volume;
					}
					if (mv & RowMaskCommand) {
						fputc(row->command,file);
						fputc(row->commandValue,file);
						lastCommand[t] = row->command;
						lastCommandValue[t] = row->commandValue;
					}
				}
			}
			fputc(0,file); //end of row
		}
    
		dataSize = ftell(file) - start;
		size_t ptr = ftell(file);
    
		fseek(file,dataSizePointer,SEEK_SET);
		fput<uint16_t>(file,dataSize);
		fseek(file,ptr,SEEK_SET);
    
		return true;
	}
  
	bool Pattern::Read(FILE* file) {
		const int ChannelMax = 64;
		uint16_t dataSize = fget<uint16_t>(file);
		uint16_t rowCount = fget<uint16_t>(file);
		this->rowCount = rowCount;
		fseek(file,4,SEEK_CUR); //reserved
		
		
		Row row;
		this->rows.resize(rowCount*ChannelMax);
		
		size_t end = ftell(file) + dataSize;
		
		uint8_t cv = 0;
		uint8_t mv = 0;
		
		uint8_t lastMv[ChannelMax];
		uint16_t lastNote[ChannelMax];
		uint8_t lastInstrument[ChannelMax];
		uint8_t lastVolume[ChannelMax];
		uint8_t lastCommand[ChannelMax];
		uint8_t lastCommandValue[ChannelMax];
		
		memset(lastMv,0,sizeof(lastMv));
		memset(lastCommandValue,0,sizeof(lastCommandValue));
		memset(lastNote,0xFF,sizeof(lastNote));
		memset(lastInstrument,0xFF,sizeof(lastInstrument));
		memset(lastVolume,0xFF,sizeof(lastVolume));
		memset(lastCommand,0xFF,sizeof(lastCommand));
		
		size_t t = 0;
		size_t i = 0;
		
		this->maxChannel = 0;
		
		while (ftell(file) < end) {
			cv = fgetc(file);
			if (cv > 0) {
				row.note = 0xFFFF;
				row.instrument = 0xFF;
				row.volume = 0xFF;
				row.command = 0xFF;
				row.commandValue = 0;	
        
				t = (cv-1) & 63;
				if (t > this->maxChannel) this->maxChannel = t;
				if (cv & 128) {
					mv = fgetc(file);
				}				
				else {
					mv = lastMv[t];
				}
				lastMv[t] = mv;
				if (mv & RowMaskNote) {
					row.note = fgetc(file);
				}
				if (mv & RowMaskInstrument) {
					row.instrument = fgetc(file);
				}
				if (mv & RowMaskVolume) {
					row.volume = fgetc(file);
				}
				if (mv & RowMaskCommand) {
					row.command = fgetc(file);
					row.commandValue = fgetc(file);
				}
				if (mv & RowMaskLastNote) {
					row.note = lastNote[t];
				}
				if (mv & RowMaskLastInstrument) {
					row.instrument = lastInstrument[t];
				}
				if (mv & RowMaskLastVolume) {
					row.volume = lastVolume[t];
				}
				if (mv & RowMaskLastCommand) {
					row.command = lastCommand[t];
					row.commandValue = lastCommandValue[t];
				}
				
				this->rows[ChannelMax*i+t] = row;
				if (row.note != 0xFFFF) lastNote[t] = row.note;
				if (row.instrument != 0xFF) lastInstrument[t] = row.instrument;
				if (row.volume != 0xFF) lastVolume[t] = row.volume;
				if (row.command != 0xFF) {
					lastCommand[t] = row.command;
					lastCommandValue[t] = row.commandValue;
				}				
			}	
			else { //end of row
				i++;		
			}
		}		
    
/*		for (int i = this->maxChannel; i >= 0; i--) {
			if (!this->channelIsEmpty(i)) break;
			this->maxChannel--;
		}*/
    
		return true;
	}
	
	bool Module::Write(FILE* file) {
		const int NameMax = 26;
		const uint32_t header = 0x4D504D49;
    
		fput<uint32_t>(file,header);
		char theName[NameMax];
		memset(theName,0,sizeof(theName));
		strcpy(theName,this->name.c_str());
		theName[NameMax-1] = '\0';
		fwrite(theName,sizeof(char),NameMax,file);
		
		fputc(this->patternHighlight[0],file);
		fputc(this->patternHighlight[1],file);
    size_t instrumentCount;
    if (this->instruments.size() == 0) {
      instrumentCount = 0;
    }
		else instrumentCount = (this->instruments.rbegin()->first)+1;
		size_t sampleCount = (this->samples.rbegin()->first)+1;
		size_t patternCount = (this->patterns.rbegin()->first)+1;
    
    if (strlen(this->message.c_str()) > 0) this->flagsSpecial |= FlagSpecialSongMessage;
    
		fput<uint16_t>(file,this->sequence.size());
		fput<uint16_t>(file,instrumentCount);
		fput<uint16_t>(file,sampleCount);
		fput<uint16_t>(file,patternCount);
		
		fput<uint16_t>(file,this->createVersion);
		fput<uint16_t>(file,this->compatVersion);
    
		fput<uint16_t>(file,this->flags);
		fput<uint16_t>(file,this->flagsSpecial);
    
		fputc(this->globalVolume,file);
		fputc(this->mixVolume,file);
		fputc(this->initSpeed,file);
		fputc(this->initTempo,file);
		fputc(this->panSeparation,file);
		fputc(this->pitchWheelDepth,file);
		
		size_t messageLength = strlen(this->message.c_str())+1;
		fput<uint16_t>(file,messageLength);
		
		size_t messagePointer = ftell(file);
		fput<uint32_t>(file,0); //placeholder
		
		fput<uint32_t>(file,0); //reserved
		
		fwrite(this->initChannelPan,sizeof(uint8_t),ChannelMax,file);
		fwrite(this->initChannelVolume,sizeof(uint8_t),ChannelMax,file);
		
		for (SequenceVector::iterator iter = this->sequence.begin(); iter != this->sequence.end(); iter++) {
			fputc(*iter,file);
		}
		
		uint32_t *instrumentPointers = new uint32_t[instrumentCount];
		memset(instrumentPointers,0,sizeof(&instrumentPointers));
		size_t instrumentPointerTable = ftell(file);
		fwrite(instrumentPointers,sizeof(uint32_t),instrumentCount,file);
		
		uint32_t *samplePointers = new uint32_t[sampleCount];
		memset(samplePointers,0,sizeof(&samplePointers));
		size_t samplePointerTable = ftell(file);
		fwrite(samplePointers,sizeof(uint32_t),sampleCount,file);
    
		uint32_t *patternPointers = new uint32_t[patternCount];
		memset(patternPointers,0,sizeof(&patternPointers));
		size_t patternPointerTable = ftell(file);
		fwrite(patternPointers,sizeof(uint32_t),patternCount,file);
    
		//fput<uint16_t>(file,0); //reserved
		
		size_t ptr = ftell(file);
		
		if (this->flagsSpecial & FlagSpecialSongMessage) {
			fseek(file,messagePointer,SEEK_SET);
			fput<uint32_t>(file,ptr);
			fseek(file,ptr,SEEK_SET);
			fwrite(this->message.c_str(),sizeof(char),messageLength,file);
			ptr = ftell(file);
		}
		
		Instrument inst; // blank instrument
		
		for (size_t i = 0; i < instrumentCount; i++) {
			instrumentPointers[i] = ptr;
			if (this->instruments.count(i) == 1) {
				this->instruments[i]->Write(file);
			}
			else {
				inst.Write(file);
			}
			ptr = ftell(file);
		}		
		
		fseek(file,instrumentPointerTable,SEEK_SET);
		fwrite(instrumentPointers,sizeof(uint32_t),instrumentCount,file);
		fseek(file,ptr,SEEK_SET);
    
		Sample samp; // blank sample
		
		for (size_t i = 0; i < sampleCount; i++) {
			samplePointers[i] = ptr;
			if (this->samples.count(i) == 1) {
				this->samples[i]->Write(file);
			}
			else {
				samp.Write(file);
			}
			ptr = ftell(file);
		}		
    
		fseek(file,samplePointerTable,SEEK_SET);
		fwrite(samplePointers,sizeof(uint32_t),sampleCount,file);
		fseek(file,ptr,SEEK_SET);
		
		for (size_t i = 0; i < patternCount; i++) {
			if (this->patterns.count(i) == 1) {
				if (!(this->patterns[i]->isEmpty() && this->patterns[i]->rowCount == 64)) {
					patternPointers[i] = ptr;	
					this->patterns[i]->Write(file);
					ptr = ftell(file);
				}
			}						
		}
    
		fseek(file,patternPointerTable,SEEK_SET);
		fwrite(patternPointers,sizeof(uint32_t),patternCount,file);
		fseek(file,ptr,SEEK_SET);
		
		delete instrumentPointers;
		delete samplePointers;
		delete patternPointers;
    
		return true;
	}
	
	bool Module::Read(FILE* file) {
		const int NameMax = 26;
		const uint32_t header = 0x4D504D49;
    
		if (fget<uint32_t>(file) != header) return false; //header mismatch
		char theName[NameMax];
		fread(theName,sizeof(char),NameMax,file);
		this->name = theName;
		
		this->maxChannel = 0;
		
		this->patternHighlight[0] = fgetc(file);
		this->patternHighlight[1] = fgetc(file);
		
		int sequenceCount = fget<uint16_t>(file);
		int instrumentCount = fget<uint16_t>(file);
		int sampleCount = fget<uint16_t>(file);
		int patternCount = fget<uint16_t>(file);
		
		this->createVersion = fget<uint16_t>(file);
		this->compatVersion = fget<uint16_t>(file);
    
		this->flags = fget<uint16_t>(file);
		this->flagsSpecial = fget<uint16_t>(file);
    
		this->globalVolume = fgetc(file);
		this->mixVolume = fgetc(file);
		this->initSpeed = fgetc(file);
		this->initTempo = fgetc(file);
		this->panSeparation = fgetc(file);
		this->pitchWheelDepth = fgetc(file);
    
		uint16_t messageLength = fget<uint16_t>(file);
		uint32_t messageOffset = fget<uint32_t>(file);
		
		fseek(file,4,SEEK_CUR); //reserved;
		
		fread(this->initChannelPan,sizeof(uint8_t),ChannelMax,file);
		fread(this->initChannelVolume,sizeof(uint8_t),ChannelMax,file);
    
		this->sequence.clear();
		
		for (int i = 0; i < sequenceCount; i++) {
			sequence.push_back(fgetc(file));
		}
    
		std::vector<uint32_t> instOffsets;
		
		for (int i = 0; i < instrumentCount; i++) {
			instOffsets.push_back(fget<uint32_t>(file));
			this->instruments[i] = new Instrument();
		}
    
		std::vector<uint32_t> sampleHeaderOffsets;
    
		for (int i = 0; i < sampleCount; i++) {
			sampleHeaderOffsets.push_back(fget<uint32_t>(file));
			this->samples[i] = new Sample();
		}
    
		std::vector<uint32_t> patternOffsets;
    
		for (int i = 0; i < patternCount; i++) {
			patternOffsets.push_back(fget<uint32_t>(file));
			this->patterns[i] = new Pattern();
		}
    
		if (this->flagsSpecial & FlagSpecialSongMessage) {
			char *buf = new char[messageLength];
			fseek(file,messageOffset,SEEK_SET);
			fread(buf,sizeof(char),messageLength,file);
			this->message = buf;
			delete buf;
		}
		else {
			this->message.clear();
		}
		
		for (InstrumentMap::iterator iter = this->instruments.begin(); iter != this->instruments.end(); iter++) {
			fseek(file,instOffsets[iter->first],SEEK_SET);
			iter->second->Read(file);
		}
    
		for (SampleMap::iterator iter = this->samples.begin(); iter != this->samples.end(); iter++) {
			fseek(file,sampleHeaderOffsets[iter->first],SEEK_SET);
			iter->second->Read(file);
		}
    
		for (PatternMap::iterator iter = this->patterns.begin(); iter != this->patterns.end(); iter++) {
			fseek(file,patternOffsets[iter->first],SEEK_SET);
			iter->second->Read(file);
			if (iter->second->maxChannel > this->maxChannel) this->maxChannel = iter->second->maxChannel;
		}
    
    
		return true;
	}
}

int DICKWEEDmain(int argc, char *argv[]) {
	FILE* inp = fopen("/Users/djbouche/Modules/slashy.it","rb");
	it::Module* mod = new it::Module();
	printf("Reading your dick...\n");
	mod->Read(inp);
	fclose(inp);
	printf("Your dick's name is: %s\n",mod->name.c_str());
	printf("It seems to have %d channels\n",mod->maxChannel);
	printf("Your dick has highlights: %d %d\n",mod->patternHighlight[0],mod->patternHighlight[1]);
	printf("Your dick has: %d sequence entries, %d instruments, %d samples, and %d patterns\n",mod->sequence.size(),mod->instruments.size(),mod->samples.size(),mod->patterns.size());
	printf("Your dick was made with %04X and compatible with %04X\n",mod->createVersion,mod->compatVersion);
	printf("Your dick has the flags %04X and special %04X\n",mod->flags,mod->flagsSpecial);
	printf("Your dick's global volume is %d and mix vol is %d\n",mod->globalVolume,mod->mixVolume);
	printf("Your dick's initial speed is %d and tempo is %d\n",mod->initSpeed,mod->initTempo);
	printf("Your dick's pan separation is %d and pitch wheel depth is %d\n",mod->panSeparation,mod->pitchWheelDepth);
	printf("Oh look, your dick has a message for you: \n%s\n",mod->message.c_str());
	
	char *buf = new char[4096];
	ArraySPrint<uint8_t>(buf,mod->initChannelPan,it::Module::ChannelMax,"%02X ");
	printf("Dick initial pan: \n%s\n",buf);
	ArraySPrint<uint8_t>(buf,mod->initChannelVolume,it::Module::ChannelMax,"%02X ");
	printf("Dick initial volume: \n%s\n",buf);
	delete buf;
	
	printf("Dick's sequence: \n");	
	for (it::SequenceVector::iterator iter = mod->sequence.begin(); iter != mod->sequence.end(); iter++) {
		printf("%02X ",*iter);
	}
	printf("\n");
	
	printf("Dick's instruments: \n");
	for (it::InstrumentMap::iterator iter = mod->instruments.begin(); iter != mod->instruments.end(); iter++) {
		char enmap[6];
		const char enchar[4] = {
			'V','P','T','F'
		};
		strcpy(enmap,"- - -");
		for (int i = 0; i < it::Instrument::EnvelopeCount; i++) {
			it::Instrument::Envelope* e = &(iter->second->envelopes[i]);
			if (e->isActive()) {
				int j = i * 2;
				int k = i;
				if (e->isFilterEnvelope()) {
					i++;
				}
				enmap[j] = enchar[k];
			}
		}
		printf("%02X (%s): %s\n",iter->first,enmap,iter->second->name.c_str());
	}
	
	printf("Dick's samples: \n");
  
	for (it::SampleMap::iterator iter = mod->samples.begin(); iter != mod->samples.end(); iter++) {
		printf("%02X: %s (Size: %08X Bytes Per Sample: %02X)\n",iter->first,iter->second->name.c_str(),iter->second->dataSize,iter->second->bytesPerSample);
	}
  
	
	printf("Dick's first pattern: \n");
	
	it::Pattern* pattern = mod->patterns[0];
	
	size_t rowCount = pattern->rows.size() / 64;
	
	size_t maxChannel = mod->maxChannel;
	
	for (int i = 0; i < rowCount; i++) {
		printf("%02X: ",i);
		for (int t = 0; t < maxChannel; t++) {
			if (!pattern->channelIsEmpty(t)) {
				it::Pattern::Row* row = &(pattern->rows[i*64+t]);
				
				if (row->note == 0xFF) {
          printf("=== ");
				}
				else if (row->note == 0xFE) {
					printf("^^^ ");
				}
				else if (row->note < 120) {
					int noteid = row->note % 12;
					int noteoct = row->note / 12;
					printf("%s%d ",it::NoteNames[noteid],noteoct);
				}
				else if (row->note < 0xFE) {
					printf("~~~ ");
				}
				else printf("    ");
				if (row->instrument != 0xFF) printf("%02X ",row->instrument);
				else printf("   ");
				if (row->volume != 0xFF) printf("%02X ",row->volume);
				else printf("   ");
				if (row->command != 0xFF) printf("%c%02X ",it::EffectNames[row->command],row->commandValue);
				else printf("    ");
				printf("|");
			}
		}
		printf ("\n");
	}
	
	
	FILE* oup = fopen("/Users/djbouche/Modules/slashy2.it","wb");
  
	printf("OHSNAP! Writing your dick...\n");
	
	mod->Write(oup);
	
	fclose(oup);
	printf("Holy fucking shit, dude. What just happened?\n");
  
	printf("Cleaning your dick...\n");
  
	delete mod;
	return 0;
}
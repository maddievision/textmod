//
//  ITModule.h
//  keyboardtest
//
//  Created by Andrew Lim on 12/05/18.
//  Copyright (c) 2012年 No Limit Zone. All rights reserved.
//


#include <vector>
#include <string>
#include <string.h>
#include <map>

template<class T> 
T fget(FILE* file) {
	T a;
	fread(&a,sizeof(T),1,file);
	return a;
}

template<class T> 
void fput(FILE* file, T value) {
	fwrite(&value,sizeof(T),1,file);
}

template<class T, class IT>
void VectorFree(T* vec) {
	for (IT iter = vec->begin(); iter != vec->end(); iter++) {
		delete *iter;
	}
}

template<class T, class IT>
void MapFree(T* vec) {
	for (IT iter = vec->begin(); iter != vec->end(); iter++) {
		delete iter->second;
	}
}

template<class T>
size_t ArraySPrint(char* dest, T* arr, size_t count, const char* fmt) {
	char* x = dest;
	for (int i = 0; i < count; i++) {
		x+=sprintf(x,fmt,arr[i]);
	}
	return x-dest;
}


namespace it {
  
  const uint16_t NoteNumberFadeStart = 120;
  const uint16_t NoteNumberCut = 254;
  const uint16_t NoteNumberOff = 255;
  const uint16_t NoteNumberNone = 0xFFFF;
  const uint8_t VPNone = 0xFF;
  const uint8_t VPVolumeStart = 0;
  const uint8_t VPVolumeRange = 65;
  const uint8_t VPFineVolumeUpStart = 65;
  const uint8_t VPFineVolumeUpRange = 10;
  const uint8_t VPFineVolumeDownStart = 75;
  const uint8_t VPFineVolumeDownRange = 10;
  const uint8_t VPVolumeSlideUpStart = 85;
  const uint8_t VPVolumeSlideUpRange = 10;
  const uint8_t VPVolumeSlideDownStart = 95;
  const uint8_t VPVolumeSlideDownRange = 10;
  const uint8_t VPPitchSlideDownStart = 105;
  const uint8_t VPPitchSlideDownRange = 10;
  const uint8_t VPPitchSlideUpStart = 115;
  const uint8_t VPPitchSlideUpRange = 10;
  const uint8_t VPPortamentoStart = 193;
  const uint8_t VPPortamentoRange = 10;
  const uint8_t VPVibratoStart = 203;
  const uint8_t VPVibratoRange = 10;
  const uint8_t VPPanStart = 128;
  const uint8_t VPPanRange = 65;
  
  const char EffectNames[31] = {
		' ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3'
	};

  enum Effects {
    EffectNone,
    EffectSpeed,                         //A
    EffectJumpToOrder,                   //B
    EffectBreakToRow,                    //C
    EffectVolumeSlide,                   //D
    EffectPitchSlideDown,                //E
    EffectPitchSlideUp,                  //F
    EffectPortamento,                    //G
    EffectVibrato,                       //H
    EffectTremor,                        //I
    EffectArpeggio,                      //J
    EffectVibratoWithVolumeSlide,        //K
    EffectPortamentoWithVolumeSlide,     //L
    EffectChannelVolume,                 //M
    EffectChannelVolumeSlide,            //N
    EffectSampleOffset,                  //O
    EffectPanSlide,                      //P
    EffectRetriggerNote,                 //Q
    EffectTremolo,                       //R
    EffectMulti,                         //S
    EffectTempo,                         //T
    EffectFineVibrato,                   //U
    EffectGlobalVolume,                  //V
    EffectGlobalVolumeSlide,             //W
    EffectPan,                           //X
    EffectPanbrello,                     //Y
    EffectMacro                          //Z
  };
  
  enum MultiEffects {
    MultiEffect0,
    MultiEffect1,
    MultiEffect2,
    MultiEffectVibratoWaveform,
    MultiEffectTremoloWaveform,
    MultiEffectPanbrelloWaveform,
    MultiEffectPatternTickDelay,
    MultiEffectInstrument,
    MultiEffectPan,
    MultiEffectSpecial,
    MultiEffectHighOffset,
    MultiEffectLoop,
    MultiEffectNoteCutAfterTickDelay,
    MultiEffectNoteTickDelay,
    MultiEffectPatternRowDelay,
    MultiEffectMacro
  };
  
  enum InstrumentMultiEffects {
    InstrumentMultiEffectPastNoteCut,
    InstrumentMultiEffectPastNoteOff,
    InstrumentMultiEffectPastNoteFade,
    InstrumentMultiEffectSetNNAToNoteCut,
    InstrumentMultiEffectSetNNAToNoteContinue,
    InstrumentMultiEffectSetNNAToNoteOff,
    InstrumentMultiEffectSetNNAToNoteFade,
    InstrumentMultiEffectVolumeEnvelopeOff,
    InstrumentMultiEffectVolumeEnvelopeOn
 };
  
  enum SpecialMultiEffects {
    SpecialMultiEffect0,
    SpecialMultiEffectSurround,
  };
  
  
  class Sample {
  public:
    
    Sample() {
      this->sampleLoaded = false;
    }
    bool Read(FILE* file);
    bool Write(FILE* file);
    bool sampleLoaded;
    
    static const uint8_t FlagHeader = 0x01;
    static const uint8_t Flag16Bit = 0x02;
    static const uint8_t FlagStereo = 0x04;
    static const uint8_t FlagCompressed = 0x08;
    static const uint8_t FlagLoop = 0x10;
    static const uint8_t FlagSustainLoop = 0x20;
    static const uint8_t FlagBidiLoop = 0x40;
    static const uint8_t FlagBidiSustainLoop = 0x80;
    
    static const uint8_t FlagConvertSigned = 0x01;
    static const uint8_t FlagConvertBigEndian = 0x02;
    static const uint8_t FlagConvertDelta = 0x04;
    static const uint8_t FlagConvertByteDelta = 0x08;
    static const uint8_t FlagConvertTX = 0x10;
    static const uint8_t FlagConvertPrompt = 0x20;
    static const uint8_t FlagConvertReserved6 = 0x40;
    static const uint8_t FlagConvertReserved7 = 0x80;
    
    std::string dosName;
    int globalVolume;
    int flags;
    int defaultVolume;
    
    std::string name;
    
    int flagsConvert;
    int defaultPan;
    
    int sampleCount;
    int loopStart;
    int loopEnd;
    uint32_t rate;
    int sustainLoopStart;
    int sustainLoopEnd;
    
    int vibratoSpeed;
    int vibratoDepth;
    
    enum VibratoWaveForm {
      VibratoWaveFormSine,
      VibratoWaveFormRampDown,
      VibratoWaveFormSquare,
      VibratoWaveFormRandom
    } vibratoWaveForm;
    
    int vibratoRate;
    
    size_t dataSize;
    size_t bytesPerSample;
    unsigned char* sampleData;
    
    void unloadSample() {
      delete this->sampleData;
    }
    
    ~Sample() {
      if (this->sampleLoaded) this->unloadSample();
    }
    
  };

  class Instrument {
  public:
    static const size_t EnvelopeTypeVolume = 0x00;
    static const size_t EnvelopeTypePanning = 0x01;
    static const size_t EnvelopeTypePitch = 0x02;
    static const size_t EnvelopeCount = 0x03;
    static const size_t NoteCount = 120;
    
    bool Read(FILE* file);
    bool Write(FILE* file);
    
    std::string dosName;	
    
    enum NewNoteAction {
      NewNoteActionCut,
      NewNoteActionContinue,
      NewNoteActionNoteOff,
      NewNoteActionNoteFade,
    } newNoteAction;
    
    enum DuplicateCheckType {
      DuplicateCheckTypeOff,
      DuplicateCheckTypeNote,
      DuplicateCheckTypeSample,
      DuplicateCheckTypeInstrument
    } duplicateCheckType;
    
    enum DuplicateCheckAction {
      DuplicateCheckTypeCut,
      DuplicateCheckTypeNoteOff,
      DuplicateCheckTypeNoteFade
    } duplicateCheckAction;
    
    
    int fadeOut;
    int pitchPanSeparation;
    int pitchPanCenter;
    int globalVolume;
    int defaultPan;
    int randomVolumeVariation;
    int randomPanVariation;
    int createVersion;
    int sampleCount;
    std::string name;
    int initFilterCutoff;
    int initFilterResonance;
    int midiChannel;
    int midiProgram;
    int midiBank;
    struct Note {
      int noteNumber;
      int sampleNumber;
    } noteSampleMap[NoteCount];
    
    class Envelope {
    public:
      
      static const uint8_t FlagOn = 0x01;
      static const uint8_t FlagLoopOn = 0x02;
      static const uint8_t FlagSustainLoopOn = 0x04;
      static const uint8_t FlagFilterOn = 0x80;
      static const size_t NodeMax = 25;
      
      bool Read(FILE* file);
      bool Write(FILE* file);
      bool isActive() {
        return (this->flags & FlagOn);
      }
      void setActive(bool val) {
        this->flags |= FlagOn;
      }
      
      bool isFilterEnvelope() {
        return (this->flags & FlagFilterOn);
      }
      bool hasLoop() {
        return (this->flags & FlagLoopOn);
      }
      bool hasSustainLoop() {
        return (this->flags & FlagSustainLoopOn);
      }
      
      
      
      int flags;
      int nodeCount;
      int loopStartIndex;
      int loopEndIndex;
      int sustainLoopStartIndex;
      int sustainLoopEndIndex;
      struct Node {
        uint8_t value;
        uint16_t tick;
      } nodes[NodeMax];
    } envelopes[EnvelopeCount];
  };

  class Pattern {
  public:
    static const uint8_t RowMaskNote = 0x01;
    static const uint8_t RowMaskInstrument = 0x02;
    static const uint8_t RowMaskVolume = 0x04;
    static const uint8_t RowMaskCommand = 0x08;
    static const uint8_t RowMaskLastNote = 0x10;
    static const uint8_t RowMaskLastInstrument = 0x20;
    static const uint8_t RowMaskLastVolume = 0x40;
    static const uint8_t RowMaskLastCommand = 0x80;
    
    bool Read(FILE* file);
    bool Write(FILE* file);
    
    size_t maxChannel;
    
    class Row {
    public:
      uint16_t note;
      uint8_t instrument;
      uint8_t volume;
      uint8_t command;
      uint8_t commandValue;
      void clearNote() { this->note = NoteNumberNone; }
      void clearInstrument() { this->instrument = 0xFF; }
      void clearVolume() { this->volume = VPNone; }
      void clearCommand() { this->command = 0xFF; this->commandValue = 0x00; }
      void clear() {
        this->clearNote();
        this->clearInstrument();
        this->clearVolume();
        this->clearCommand();
      }
      Row() {
        this->clear();
      }
      void setCommand( char cmd, uint8_t val ) {
        for (int i = 0; i < 31; i++) {
          if (EffectNames[i] == cmd) {
            this->command = i;
            break;
          }
        }
        this->commandValue = val;
      }
      bool hasNote() { return this->note != NoteNumberNone; }
      bool hasVolume() { return this->volume != VPNone; }
      bool hasInstrument() { return this->instrument != 0x00 && this->instrument != 0xFF; }
      bool hasCommand() { return this->command != 0xFF && this->command != EffectNone; }
      bool isEmpty() {
        return !this->hasNote() && !this->hasVolume() && !this->hasInstrument() && !this->hasCommand();
      }
    };
    
    
    size_t rowCount;
    
    bool isEmpty() {
      for (size_t i = 0; i < 64; i++) {
        if (!this->channelIsEmpty(i)) return false;
      }
      return true;
    }
    
    bool channelIsEmpty(size_t channelNumber) {
      for (size_t i = 0; i < this->rowCount; i++) {
        if (!this->rows[i*64+channelNumber].isEmpty()) return false;
      }
      return true;
    }
    
    Row* row(size_t channel, size_t rowNumber) {
      return &(this->rows[rowNumber*64+channel]);
    }
    
    void setSize(size_t rowCount) {
      this->rows.clear();
      this->rowCount = rowCount;
      for (size_t i = 0; i < (rowCount*64); i++) {
        Row row; //row your boat
        this->rows.push_back(row);
      }
    }
    
    
    std::vector<Row> rows;
  };

  typedef std::vector<uint8_t> SequenceVector;
  typedef std::map<size_t,Sample*> SampleMap;
  typedef std::map<size_t,Instrument*> InstrumentMap;
  typedef std::map<size_t,Pattern*> PatternMap;


  class Module {
  public:
    
    static const uint16_t ChannelMax = 64;
    
    static const uint16_t FlagStereo = 0x01;
    static const uint16_t FlagVol0MixOptimizations = 0x02;
    static const uint16_t FlagUseInstruments = 0x04;
    static const uint16_t FlagLinearSlides = 0x08;
    static const uint16_t FlagOldEffects = 0x10;
    static const uint16_t FlagLinkPortamento = 0x20;
    static const uint16_t FlagMIDIPitchController = 0x40;
    static const uint16_t FlagEmbeddedMIDIConfig = 0x80;
    
    static const uint16_t FlagSpecialSongMessage = 0x01;
    static const uint16_t FlagSpecialReserved1 = 0x02;
    static const uint16_t FlagSpecialReserved2 = 0x04;
    static const uint16_t FlagSpecialMIDIConfigEmbedded = 0x08;
    static const uint16_t FlagSpecialReserved4 = 0x10;
    static const uint16_t FlagSpecialReserved5 = 0x20;
    static const uint16_t FlagSpecialReserved6 = 0x40;
    static const uint16_t FlagSpecialReserved7 = 0x80;
    
    Module() { } // blank constructor
    
    size_t maxChannel;
    
    bool Read(FILE* file);
    bool Write(FILE* file);
    std::string name;
    int patternHighlight[2];
    int createVersion; //= 0x217;
    int compatVersion; //= 0x200;
    uint16_t flags; 
    // = kFlagStereo + kFlagUseInstruments + kFlagLinearSlides + kFlagLinkPortamento;
    uint16_t flagsSpecial; 
    // = kFlagSpecialSongMessage + kFlagSpecialReserved1 + kFlagSpecialReserved2;
    int globalVolume;
    int mixVolume;
    int initSpeed;
    int initTempo;
    int panSeparation;
    int pitchWheelDepth;
    std::string message;
    uint8_t initChannelPan[ChannelMax];
    uint8_t initChannelVolume[ChannelMax];
    SequenceVector sequence;
    SampleMap samples;
    InstrumentMap instruments;
    PatternMap patterns;
    void clearSamples() {
      MapFree<SampleMap,SampleMap::iterator>(&(this->samples));
    }
    void clearInstruments() {
      MapFree<InstrumentMap,InstrumentMap::iterator>(&(this->instruments));
    }
    void clearPatterns() {
      MapFree<PatternMap,PatternMap::iterator>(&(this->patterns));
    }
    ~Module() {
      MapFree<SampleMap,SampleMap::iterator>(&(this->samples));
      MapFree<InstrumentMap,InstrumentMap::iterator>(&(this->instruments));
      MapFree<PatternMap,PatternMap::iterator>(&(this->patterns));
    }
  };
}
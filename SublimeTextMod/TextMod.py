import sublime, sublime_plugin

kbm = {
  'q' : ('c',0),
  '2' : ('c#',0),
  'w' : ('d',0),
  '3' : ('d#',0),
  'e' : ('e',0),
  'r' : ('f',0),
  '5' : ('f#',0),
  't' : ('g',0),
  '6' : ('g#',0),
  'y' : ('a',0),
  '7' : ('a#',0),
  'u' : ('b',0),
  'i' : ('c',1),
  '9' : ('c#',1),
  'o' : ('d',1),
  '0' : ('d#',1),
  'p' : ('e',1),

  'z' : ('c',-1),
  's' : ('c#',-1),
  'x' : ('d',-1),
  'd' : ('d#',-1),
  'c' : ('e',-1),
  'v' : ('f',-1),
  'g' : ('f#',-1),
  'b' : ('g',-1),
  'h' : ('g#',-1),
  'n' : ('a',-1),
  'j' : ('a#',-1),
  'm' : ('b',-1)
}


class TextModBaseCommand(sublime_plugin.TextCommand):  
  def get_octave(self):
    if self.view.settings().get("text_mod_octave") == None:
      self.view.settings().set("text_mod_octave",5)
    return self.view.settings().get("text_mod_octave")
  def set_octave(self,new_octave):
    self.view.settings().set("text_mod_octave",new_octave)
  def octave_up(self):
    self.set_octave(self.get_octave()+1)
  def octave_down(self):
    self.set_octave(self.get_octave()-1)

  def set_note_mode(self, new_mode):
    self.view.settings().set("text_mod_note_mode", new_mode)


  def map(self, c):
    if c not in kbm: return c
    n = kbm[c][0]
    o = kbm[c][1]

    o += self.get_octave()
    ts = "%s%d" % (n,o)
#    while len(ts) < 4: ts += ""
    return ts

class InsertTextModNoteCommand(TextModBaseCommand):
  def run(self, edit, key):
    for r in self.view.sel():
      x = ""
      if key in "./\\":
        x = key
      else:
        x = self.map(key)

      self.view.insert(edit,r.a,x + "\t")

class EnterTextModNoteModeCommand(TextModBaseCommand):
  def run(self, edit):
    self.set_note_mode(True)

class ExitTextModNoteModeCommand(TextModBaseCommand):
  def run(self, edit):
    self.set_note_mode(False)

class TextModOctaveUpCommand(TextModBaseCommand):
  def run(self, edit):
    self.octave_up()
class TextModOctaveDownCommand(TextModBaseCommand):
  def run(self, edit):
    self.octave_down()

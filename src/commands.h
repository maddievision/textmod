#pragma mark Commands
const int cmdcount = 89;

const char cmdlist[cmdcount][10] = {
  //Blocks 5
  "INIT", "PACK", "NAME", "NEXT", "END", "OUT", "", 
  //Globals 6
  "RAND", "SPEED", "TEMPO", "GVOL", "ROWS", "PPQN", "",
  //Pattern 3
  "LOOP", "ONLY", "RECALL", "",
  //Addressing 6
  "CH", "CV", "SUB",  "(", ")", ":", "{", "}", "@", "",
  //Channel Inits 11
  "SOLO", "VOL", "PAN", "BC",  "BV", "BF", "BR", "BD", "BT", "BI", "",
  //data commands 9
  "%", "s", "$G", "$F", "$H", "$Z", "r", "$nr", "$tm", "$ga", "$T", "$v", "v","l","_","!","$d","'","",
  //short data commands 2
  "o","",
  //data triggers 3
  "<",">","[", "]","|","CUT","",
  //notes 12
  "n","c","d","e","f","g","a","b",".","\"","/","\\","",
  //spec 5
  "p+","p-","~","&","",
  //triggers 3
  "ERASE","%rg", ""
};

#pragma mark enums

enum cmdtype {
  CT_BLOCK,CT_GLOBAL,CT_PATTERN,CT_ADDRESSING,CT_CHANNELINIT,
  CT_DATA,CT_SHORT,CT_DATATRIGGER,CT_NOTES,CT_SPEC,CT_TRIGGERS
};
enum cmds_block {
  CMD_BLOCK_INIT,CMD_BLOCK_PACK,CMD_BLOCK_NAME,CMD_BLOCK_DATA,CMD_BLOCK_ENDM,CMD_BLOCK_OUTF
};
enum cmds_globals {
  CMD_GLOBAL_RAND,CMD_GLOBAL_IS,CMD_GLOBAL_IT,CMD_GLOBAL_IG,CMD_GLOBAL_PR,CMD_GLOBAL_PPQN
};
enum cmds_pattern {
  CMD_PATTERN_LOOP,CMD_PATTERN_ONLY,CMD_PATTERN_RP
};
enum cmds_addressing {
  CMD_ADDRESSING_CH,CMD_ADDRESSING_GA,CMD_ADDRESSING_SB,CMD_ADDRESSING_OPEN_PAREN,
  CMD_ADDRESSING_CLOSE_PAREN,CMD_ADDRESSING_COLON,CMD_ADDRESSING_OPEN_CURL,CMD_ADDRESSING_CLOSE_CURL,
  CMD_ADDRESSING_AT 
};
enum cmds_channelinit {
  CMD_CHANNELINIT_SOLO,
  CMD_CHANNELINIT_IV,  CMD_CHANNELINIT_IP,
  CMD_CHANNELINIT_BC,  CMD_CHANNELINIT_BV,  CMD_CHANNELINIT_BF,
  CMD_CHANNELINIT_BR,  CMD_CHANNELINIT_BD,  CMD_CHANNELINIT_BT,
  CMD_CHANNELINIT_BI
};
enum cmds_data {
  CMD_DATA_PERCENT, CMD_DATA_SB, CMD_DATA_G,  CMD_DATA_F,  CMD_DATA_H,
  CMD_DATA_Z,  CMD_DATA_RS,  CMD_DATA_NR,
  CMD_DATA_TM,  CMD_DATA_GA, CMD_DATA_TR, CMD_DATA_IV, CMD_DATA_V , CMD_DATA_L,
  CMD_DATA_UNDERSCORE,CMD_DATA_EXC,CMD_DATA_SS,CMD_DATA_QUO
};
enum cmds_short {
  CMD_SHORT_O
};
enum cmds_datatrigger {
  CMD_DATATRIGGER_LESS_THAN,  CMD_DATATRIGGER_GREATER_THAN,
  CMD_DATATRIGGER_OPEN_SQBR,CMD_DATATRIGGER_CLOSE_SQBR,CMD_DATATRIGGER_PIPE,CMD_DATATRIGGER_CUT
};
enum cmds_notes {
  CMD_NOTE_N,
  CMD_NOTE_C,  CMD_NOTE_D,  CMD_NOTE_E,  CMD_NOTE_F,
  CMD_NOTE_G,  CMD_NOTE_A,  CMD_NOTE_B,
  CMD_NOTE_DOT,  CMD_NOTE_SLASH,  CMD_NOTE_CARET, CMD_NOTE_BACKSLASH
};
enum cmds_spec {
  CMD_SPEC_PU,  CMD_SPEC_PD,
  CMD_SPEC_TILDE,  CMD_SPEC_AMPERSAND,
};
enum cmds_trigger {
  CMD_TRIGGER_CLEAR,CMD_TRIGGER_RG
};
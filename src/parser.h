#include "commands.h"
#pragma mark String Handling

#define clearstr(strname) memset(strname,0,256)
#define newstr(strname)  \
char strname[256]; \
memset(strname,0,256);


#pragma mark Value Scanning

#pragma mark ForEach

#define foreach( i, c )\
typedef __typeof__( c ) c##_CONTAINERTYPE;\
for( c##_CONTAINERTYPE::iterator i = c.begin(); i != c.end(); ++i )


bool is_whitespace(char t) {
  return (t == '\t' || t == '\n' || t == '\r' ||
          t == ' ' || t == '=');
}

bool is_hex_character(char t) {
  return (t >= '0' && t <= '9') || (t >= 'A' && t <= 'F');
}

bool is_num_character(char t) {
  return (t >= '0' && t <= '9');
}

struct mcmd {
  int ctype;
  int cid;
  int cval;
  bool csign;
  bool cmul;
  bool cdiv;
  bool hasval;
  bool rowend;
  bool csharp;
  char cchar;
  mcmd():
  ctype(-1),cid(-1),cval(0),csign(false),cmul(false),cdiv(false),hasval(false),rowend(false),
  csharp(false),cchar(' '){}
  
  void parse(const char* str) {
    this->ctype = -1;
    this->cid = -1;
    int cmdscantype = 0;
    int cmdscanid = 0;
    for (int i = 0; i < cmdcount; i++) {
      newstr(x);
      strcpy(x,cmdlist[i]);
      if (strlen(x) < 1) {
        cmdscantype++;
        cmdscanid = 0;
      }
      else if (strcmp(str,x) == 0) {
        this->ctype = cmdscantype;
        this->cid = cmdscanid;
        return;
      }
      else {
        cmdscanid++;
      }
    }
  }

  void applyval(int* destval) {
    if (this->csign) { //relative
      if (this->cmul) {
        (*destval) = (*destval) * this->cval;
      }
      else if (this->cdiv) {
        (*destval) = (*destval) / this->cval;
      }
      else {
        (*destval) = (*destval) + this->cval;
      }
    }
    else { //absolute
      (*destval) = this->cval;
    }
  }
  
  void scansharp(char* strname,int* counter) {
    int ln = 0;
    this->csharp = false;
    int c = *counter;
    if (strname[c] == '#') {
      this->csharp = true;
      c++;
    }
    *counter = c;
  }
  
   void scanletter(char* strname,int* counter) {
    int ln = 0;
    this->cchar = ' ';
    int c = *counter;
    if (strname[c] >= 'A' && strname[c] <= 'Z') {
      this->cchar = strname[c];
      c++;
    }
    *counter = c;
  }
  
  
  void scanval(char* strname,int* counter) {
    newstr(scanstrname);


    bool hex = false;
    int ln = 0;
    this->cval = 0;
    this->csign = false;
    this->cmul = false;
    this->cdiv = false;
    this->hasval = false;
    int c = *counter;
    int mult = 1;
    
    //eat whitespace
    while (is_whitespace(strname[c])) c++;

/*    if (strname[c] == '*') {
      this->cmul = true;
      c++;
    }
    else if (strname[c] == '/') {
      this->cdiv = true;
      c++;
    }*/
    
    if (strname[c] == '+') {
      c++;
    }
    else if (strname[c] == '-') {
      mult = -1;
      c++;
    }
    else if (strname[c] == 'L') {
      mult = -1;
      c++;
    }
    else if (strname[c] == 'R') {
      c++;
    }

    if (strname[c] == '=') {
      this->csign = true;
      c++;
    }

    if (strname[c] == 'x') {
      hex = true;
      c++;
    }
    
    while (ln < 3) {
      if ((hex && is_hex_character(strname[c])) ||
          is_num_character(strname[c])) {
        scanstrname[ln++] = strname[c++];
        scanstrname[ln] = 0;
        this->hasval = true;
      }
      else {
        break;
      }
    }
    if (hex) {
      sscanf(scanstrname,"%X",&this->cval);
    }
    else {
      sscanf(scanstrname,"%d",&this->cval);
    }
    this->cval *= mult;
    *counter = c;
  }

};

struct mcmdcontainer {
  int loop_index;
  bool retrig;
  std::vector<mcmd> d;
  mcmdcontainer():
  loop_index(-1),retrig(false) {}
};

struct mcmdpattern {
  int itpatindex,itrowindex;
  mcmdcontainer c[128];
  mcmdpattern():itpatindex(-1),itrowindex(-1) {}
};

struct mglobalinfo {
  int init_row_speed,init_tempo,init_global_vol,pattern_rows;
  int row_speed,tempo,global_vol,loop_index,ppqn;
  std::string name;
  bool randomize;
  mglobalinfo():
  init_row_speed(2),init_tempo(125),init_global_vol(100),pattern_rows(96),
  row_speed(2),tempo(125),global_vol(100),loop_index(-1),ppqn(24),
  randomize(false) {}
};

struct mloopinfo {
  int start_index,coda_trigger_index,end_index,loop_count;
  mloopinfo():start_index(-1),coda_trigger_index(-1),end_index(-1),loop_count(-1) {}
};

struct msubinfo {
  mcmdcontainer* ctr;
  bool drum;
  int restore_oct;
  int sub_index;
  int cmd_index;
  msubinfo():sub_index(-1),cmd_index(-1),drum(false),restore_oct(-1) {}
};

struct mchinfo {
  bool active;
  int clone_channel,clone_vol,clone_filt,clone_row_offset,clone_tick_offset;
  int clone_transpose,clone_inst;
  int init_vol,init_pan;
  
  mchinfo():
  active(false),
  clone_channel(-1),clone_vol(64),clone_filt(64),clone_row_offset(0),clone_tick_offset(0),
  clone_transpose(0),clone_inst(0),init_vol(48),init_pan(0) {}
  
  struct mnotesettings {
    int note_vol,init_note_vol,port_speed,pitch_speed,vib_setting,octave,current_note,autoport;
    int filter, row_skip, skip_index, inst;
    int active_gate,gate_cmd_index;
    int cmd_index,transpose;
    int note_off_counter; int note_off_value;
    bool vibrato_enabled,port_enabled;
    int pitch_direction,fine_note,dest_fine_note,pitch_step;
    bool finished,breaking;
    std::vector<mloopinfo> loops;
    std::vector<msubinfo> subs;
    mnotesettings():
    note_vol(64),init_note_vol(64),autoport(0),port_speed(0xA0),pitch_speed(0x02),vib_setting(0x66),octave(5),current_note(-1),pitch_step(0),pitch_direction(0),
    filter(0x7F),row_skip(6),skip_index(0),inst(1),note_off_counter(-1),note_off_value(0),
    active_gate(-1),gate_cmd_index(0),
    cmd_index(0),finished(false),transpose(0),breaking(false) {}
  } notesettings;
};

enum addressmodes {
  ADDRESS_MODE_NONE,
  ADDRESS_MODE_GLOBAL,
  ADDRESS_MODE_CHANNEL,
  ADDRESS_MODE_GATE,
  ADDRESS_MODE_SUB,
  ADDRESS_MODE_PATTERN
};
enum readmode {
  READ_MODE_CMD
};
enum blockmode {
  BLOCK_MODE_NONE,
  BLOCK_MODE_READY,
  BLOCK_MODE_INIT,
  BLOCK_MODE_DATA
};




